#include "Helper.h"
#include <Windows.h>
#include <iostream>
#include <string>
#include <fstream>  
#include <tchar.h>

#define PATH_MAX 2048
typedef std::vector<std::string> stringvec;
typedef int(__cdecl *MYPROC)(LPWSTR);

Helper h;
std::vector<std::string> getFromUser(); // Question 1
void pwd(); // Question 2
void cd(std::string path); // Question 3
void create(std::string fileName); //Question 4
void ls(); // Question 5
void read_directory(const std::string& name, stringvec& v);
//void secret(); // Question 6
void executable(std::string input); // Question 7
bool checkNumParameters(std::vector<std::string> input);

int main()
{
	bool ans;
	std::vector<std::string> input = getFromUser();

	while (input[0].compare("exit") != 0)
	{
		if (input[0].compare("pwd") == 0)
		{
			pwd();	
		}
		else if (input[0].compare("cd") == 0)
		{
			ans = checkNumParameters(input);
			if (ans)
			{
				cd(input[1]);
			}
		}
		else if (input[0].compare("create") == 0)
		{
			ans = checkNumParameters(input);
			if (ans)
			{
				create(input[1]);
			}
		}
		else if(input[0].compare("ls") == 0)
		{
			ls();
		}
		else if (input[0].compare("secret") == 0)
		{
			secret();
		}
		else if (input[0].find(".exe"))
		{
			executable(input[0]);
		}
		else
		{
			std::cout << "Failed to create process\nerror:2" << std::endl;
		}

		input = getFromUser();
	}

	system("PAUSE");
	return 0;
}

/*
	Input: none
	Output: The input
	The function scans the input the user typed and returns it
*/
std::vector<std::string> getFromUser()
{
	std::string str = "";
	std::cout << ">>";
	std::getline(std::cin, str); // Get input from user
	h.trim(str); // Delete all the spaces from the string
	return h.get_words(str); // Seperate the string into words in a vector and return it
}

/*
	Input: none
	Output: none
	The function prints the current directory path
*/
void pwd()
{
	char p[PATH_MAX];
	GetCurrentDirectory(PATH_MAX, p);
	std::cout << p << std::endl;
}

/*
	Input: a path
	Output: none
	The function sets the current directory path into the parameter path
*/
void cd(std::string path)
{
	SetCurrentDirectory(path.c_str());
}

/*
	Input: a file name
	Output: none
	The function creates the file based on the file name and ending
*/
void create(std::string fileName)
{
	std::ofstream file(fileName);
}

/*
	Input: none
	Output: none
	The function prints the list of the files in the current directory path
*/
void ls()
{
	stringvec v;
	read_directory(".", v);
	std::copy(v.begin(), v.end(),
	std::ostream_iterator<std::string>(std::cout, "\n"));
}

/*
	The function reads all the directory files and details
*/
void read_directory(const std::string& name, stringvec& v)
{
	std::string pattern(name);
	pattern.append("\\*");
	WIN32_FIND_DATA data;
	HANDLE hFind;
	if ((hFind = FindFirstFile(pattern.c_str(), &data)) != INVALID_HANDLE_VALUE) {
		do {
			v.push_back(data.cFileName);
		} while (FindNextFile(hFind, &data) != 0);
		FindClose(hFind);
	}
}

/*
	Input: none
	Output: none
	The function opens the dll and runs the function TheAnswerToLifeTheUniverseAndEverything
*/
void secret()
{
	HINSTANCE hinstLib;
	MYPROC ProcAdd;

	// Get a handle to the DLL module.
	hinstLib = LoadLibrary(TEXT("Secret.dll"));

	if (hinstLib != NULL) // If the handle is valid, try to get the function address.
	{
		ProcAdd = (MYPROC)GetProcAddress(hinstLib, "TheAnswerToLifeTheUniverseAndEverything");

		if (NULL != ProcAdd) // If the function address is valid, call the function.
		{
			std::cout << (ProcAdd)(L"") << std::endl;
		}

		// Free the DLL module.
		FreeLibrary(hinstLib);
	}
}

/*
	Input: the vector of the input
	Output: true or false
	The function returns false if the size of the vector is different than 2 and prints a suitable message, otherwise - return true
*/
bool checkNumParameters(std::vector<std::string> input)
{
	bool ans = true;

	if (input.size() != 2)
	{
		std::cout << "Wrong parameter number\nExpected:2 Actual:" << input.size() << std::endl;
		ans = false;
	}

	return ans;
}

/*
	Input: the name of the exe file
	Output: none
	The function runs the exe file
*/
void executable(std::string input)
{
	STARTUPINFO si;
	PROCESS_INFORMATION pi;

	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));

	// Start the child process. 
	if (!CreateProcess(input.c_str(),   // No module name (use command line)
		NULL,        // Command line
		NULL,           // Process handle not inheritable
		NULL,           // Thread handle not inheritable
		FALSE,          // Set handle inheritance to FALSE
		0,              // No creation flags
		NULL,           // Use parent's environment block
		NULL,           // Use parent's starting directory 
		&si,            // Pointer to STARTUPINFO structure
		&pi)           // Pointer to PROCESS_INFORMATION structure
		)
	{
		printf("CreateProcess failed (%d).\n", GetLastError());
		return;
	}

	// Wait until child process exits.
	WaitForSingleObject(pi.hProcess, INFINITE);

	// Close process and thread handles. 
	CloseHandle(pi.hProcess);
	CloseHandle(pi.hThread);
}

